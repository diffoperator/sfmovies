/**
 * Module dependencies.
 */

var express        = require('express');
var fs             = require('fs');
var http           = require('http');
var path           = require('path');
var engine         = require('ejs-locals');
var cookieParser   = require('cookie-parser');
var bodyParser     = require('body-parser');
var methodOverride = require('method-override');
var morgan         = require('morgan');
var errorHandler   = require('errorhandler');
var static         = require('serve-static');

var app            = express();
var routes         = require('./routes');

// Mongoose
var Mongoose = require('mongoose');
var db = Mongoose.createConnection('localhost', 'sfmovies');

var MovieSchema = require('./models/Movie');
var Movie = db.model('movies', MovieSchema.Movie);

// all environments
app.set('port', process.env.PORT || 3001 );
app.engine('ejs', engine );
app.set('views', path.join( __dirname,'views'));
app.set('view engine','ejs');

// Set up the logger
var accessLogStream = fs.createWriteStream(__dirname + '/app.log', {flags: 'a'});
app.use(morgan('combined', {stream: accessLogStream}));

app.use(methodOverride());
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended : true }));

// Routes
//app.use( routes.current_user );
app.get('/', routes.index());

// Read
app.get('/movie', routes.getMovieInfoByName(Movie));

app.get('/movie/location', routes.getMovieLocationByName(Movie));

app.get('/movie/release', routes.getMovieByRelease(Movie));

app.get('/movie/studio', routes.getMovieByStudio(Movie));

app.get('/movie/all', routes.getAllMovies(Movie));

app.get('/movie/all/names',routes.getAllMovieNames(Movie));

app.get('/movie/location/radius', routes.getMoviesInRadius(Movie));

app.use( static( path.join( __dirname,'public')));

// development only
if('development'== app.get('env')){
    app.use( errorHandler());
}

http.createServer( app ).listen( app.get('port'), function (){
	console.log('Express server listening on port'+ app.get('port'));
    });
