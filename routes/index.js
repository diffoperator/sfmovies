/**
 * Controller routes
 */
var __ = require('underscore');

var mongoose = require('mongoose');

exports.index = function() {
    return function(req, res) {
	return res.render('index', {
		title: 'Challenge'
	    });
    };
};

exports.getMovieInfoByName = function(Movie) {
    return function(req, res) {
	var name = req.query.name;

	Movie.find({ name : name}, function(error, movies) {
		if (movies != null && error == null) {
		    return res.json(movies);
		} else {
		    if (error != null) {
			return res.json({ error : error });
		    } else {
			return res.json({ error : "No records" });
		    }
		}
	    });
    };
};

exports.getMovieLocationByName = function(Movie) {
    return function(req, res) {
	var name = req.query.name;

	Movie.find({ name : name}, function(error, movies) {
		if (movies != null && error == null) {
		    return res.json({ locations : __.map(movies, function(movie) {
				    return {
					"location" : movie["location"],
					    "address": movie["address"]
					    };
				})});
		} else {
		    if (error != null) {
			return res.json({ error : error });
		    } else {
			return res.json({ error : "No records" });
		    }
		}
	    });
    };
};

exports.getMovieByRelease = function(Movie) {
    return function(req, res) {
	var release = parseInt(req.body.release);

	Movie.find({ release : release }, function(error, movies) {
		if (error != null) {
		    return res.json({ error : error });
		} else {
		    return res.json({ movies : movies });
		}
	    });
    };
};

exports.getMovieByStudio = function(Movie) {
    return function(req, res) {
	var studio = req.body.studio;

	Movie.find({ studio : studio }, function(error, movies) {
		if (error != null) {
		    return res.json({ error : error });
		} else {
		    return res.json({ movies : movies });
		}
	    });
    };
};

exports.getAllMovies = function(Movie) {
    return function(req, res) {
	Movie.find({}, function(err, movies) {
		if (err != null) {
		    return res.json({ error : err });
		}

		return res.json({ movies : movies });
	    });
    };
};

exports.getAllMovieNames = function(Movie) {
    return function(req, res) {
	Movie.find({}, function(err, movies) {
		if (err != null) {
		    return res.json({ error : err });
		}

		return res.json({ movies : __.uniq(__.map(movies, __.iteratee('name'))) })
	    });
    };
};

exports.getMoviesInRadius = function(Movie) {
    return function(req, res) {
	Movie.find({ location : { $near : [parseFloat(req.query.latitude), parseFloat(req.query.longitude)] }})
	.limit(req.query.limit != null ? req.query.limit : 1)
	.exec(function(err, movies) {
		if (err != null) {
		    return res.json({ error : err });
		}

		return res.json({ movies : movies });
	});
    };
};
