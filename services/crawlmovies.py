import datetime
import time
import json
import requests
import os
from pymongo import MongoClient
from pygeocoder import Geocoder
import logging

class MovieCrawler():
    """
    The crawler downloads the dataset locally and checks which entries
    are new or have been updated.
    It then resolves the address from Google Maps API and chucks it into
    the database.
    """
    def __init__(self, db="default", *args):
        self.client   = MongoClient('mongodb://localhost:27017/' + db)
        self.log      = logging.getLogger('crawlmovies')
        fh = logging.FileHandler('crawler.log')
        self.log.addHandler(fh)
        self.url      = 'https://data.sfgov.org/api/views/yitu-d5am/rows.json?accessType=DOWNLOAD'

    def insert(self, info):
        db = self.client['sfmovies']
        movies = db.movies
        movie_id = movies.insert(info)

    def fetch(self):
        local_filename = 'sfdata.json'
        r = requests.get(self.url)
        with open(local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024): 
                if chunk:
                    f.write(chunk)
                    f.flush()
                    os.fsync(f.fileno())
        return

    def resolve(self):
        try:
            data = json.load(file('./sfdata.json'))
        except Exception, e:
            self.log.error("Could not parse data file: %s", e)
            return

        for row in data['data']:
            name = row[8].strip()
            release = int(row[9])
            address = row[10]
            studio = row[12]

            if address is None or name is None:
                continue

            try:
                resolved_location = Geocoder.geocode(address + ', San Francisco, California')
                coords = resolved_location[0].coordinates
                formatted_address = str(resolved_location[0])
                info = {
                    "name": name,
                    "release": release,
                    "location": list(coords),
                    "address": formatted_address,
                    "studio": studio,
                    "created_at": datetime.datetime.utcnow(),
                    "updated_at": datetime.datetime.utcnow()
                    }
                self.insert(info)
                self.log.debug(info)

                # We do not want to be throttled by Google
                time.sleep(12)
            except Exception, e:
                self.log.error("Geocoding is not working: %s", e)
                continue

if __name__ == '__main__':
    m = MovieCrawler()
    m.fetch()
    m.resolve()
