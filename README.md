Uber challenge app

Problem: 
========
Map out locations of movies on a map and provide an autocomplete filter
to make it easier.

Solution: 
========
Implement a nodejs app using express which is backed by Mongo and served by nginx.
Use a separate crawler to get data from sfdata.gov and resolve all the addresses before
hand (the dataset does not change that often) to their respective coordinates.
This is better than client side resolution because geocoding is an expensive process.
It also allows for better caching because instead of maintaining individual client side
caches, we can just enable a server side cache that can absorb all requests without
ever having to hit the nodejs process.

Focus:
========

The focus is mostly backend, however since I had experience with front end work as well
I decided to add in a little bit of frontend work. I have experience with AngularJS and
React and not Backbone and circumstances did not allow me enough time to experiment with
it.

Technologies used:
==================

Nodejs, Express, Mongo and Python for crawler. Various other libraries here and there.

Reasoning on tradeoffs:
======================

There are no major tradeoffs I made except for how the geocoding is done. While it could
have been client side and would have been easier to implement, as explained earlier, it
is a poor choice because results cannot be cached (even though they are not session specific)
and will fail if the Maps API is taken down, for whatever reason. In this event, since we
already have the data stored and cached if we do server side geocoding, we will be in a much
better position.

There are other things that I had to pass on, such as comprehensive testing, redundancy and
failover (what if our service fails? we should do client side resolving then), comprehensive
caching solution and other UX features (especially multi select, which I had planned on
implementing). As one might have noticed, the addresses given in the dataset are horrible
and one probably needs to clean them up. However I could not find any trivial solution to the
problem and this probably requires deeper thought.

I have implemented backend code to fetch titles based on release year, production
house and radius search. If I had additional time, I would definitely work on finishing off
these features.

I had several other ideas for improvement that I can discuss in person.

Link:
=====

http://54.69.225.64/

Other code:
===========

Email sent to contacting recruiter with several code samples and open source projects

Experience with framework:
==========================

First time writing a MEAN stack application. I chose nodejs because I heard it is used in
Uber and I wanted to experiment with a completely event driven framework.

Security
========

Pretty much non existent. Still a Nodejs sec n00b.
