$(function() {
    // Set up autocomplete
    $.get('/movie/all/names', function(data) {
        $('#tags').autocomplete({
	    source: data['movies'],
	    select: function(event, ui) {
	        $('#selectable .ui-selected').removeClass('ui-selected');
		movieSelect(ui["item"]["value"]);
	    }
	});

	$.each(data['movies'], function(i, movie) {
	    movie = movie.length > 40 ? movie.substring(0, 40) + "..." : movie;
	    var item = $('<li></li>').attr('class', 'ui-widget-content').text(movie);
	    $('#selectable').append($(item));
	});
    });

    // Initialize Google Maps
    initGoogleMaps();

    // Set up selectable
    $( "#selectable" ).selectable({
	selected: function(event, ui) {
	    var selectedMovie = ui['selected'].textContent;
	    movieSelect(selectedMovie);
	}
    });
});

var map = {};

function initGoogleMaps() {
    map.mapCanvas = document.getElementById('map-canvas');
    map.mapOptions = {
	center: new google.maps.LatLng(37.7833, -122.4167),
	zoom: 12,
	mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map.map = new google.maps.Map(map.mapCanvas, map.mapOptions);
    map.markers = [];
}

function movieSelect(movieName) {
    $.get('/movie/location', { 'name' : movieName }, function(data) {
	    clearMarkers();
	    showOnMap(data["locations"]);
	});
}

function clearMarkers() {
    $.each(map.markers, function(i) {
	map.markers[i].setMap(null);
    });
    map.markers = [];
}

function showOnMap(locations) {
    var marker;

    var infowindow = new google.maps.InfoWindow();

    $.each(locations, function(i) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i]["location"][0], locations[i]["location"][1]),
            map: map.map
        });

	map.markers.push(marker);

        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent(locations[i]["address"]);
                infowindow.open(map.map, marker);
            }
	})(marker, i));
    });
}
