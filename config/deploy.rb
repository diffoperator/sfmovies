# This defines a deployment "recipe" that you can feed to capistrano
# (http://manuals.rubyonrails.com/read/book/17). It allows you to automate
# (among other things) the deployment of your application.

# =============================================================================
# REQUIRED VARIABLES
# =============================================================================
# You must always specify the application and repository for every recipe. The
# repository must be the URL of the repository you want this recipe to
# correspond to. The deploy_to path must be the path on each machine that will
# form the root of the application path.

set :application, "UberChallenge"

# Source code
default_run_options[:pty] = true
set :repository, "https://diffoperator@bitbucket.org/diffoperator/sfmovies.git"
set :scm, :git
set :repository_cache, "git_cache"
set :deploy_via, :remote_cache
set :keep_releases, 2

# =============================================================================
# OPTIONAL VARIABLES
# =============================================================================
set :deploy_to, "/home/ubuntu/#{application}" # defaults to "/u/apps/#{application}"
ssh_options[:forward_agent] = true
# set :use_sudo, false
set :user, 'ubuntu'

desc "Run tasks in production environment."
task :production do
  set :port, 22
  set :branch, "master"
  set :environment, 'production'
  role :web, "54.69.225.64"
  set :allow_push_db, false
  set :show_git_tag, false
end

# =============================================================================
# BACKGROUND JOBS / MINION JOBS
# =============================================================================
# An array of arrays, key 0 is the task name, key 1 is the number of workers to start.
tasks = [
  ['jobs:sec:formd', 1],
]

# =============================================================================
# ADDITIONAL TASKS
# =============================================================================
after "deploy", "deploy:cleanup"
before "deploy", "deploy:branch_select"
after "deploy", "deploy:cron"
after "deploy", "deploy:migrations"

namespace :deploy do

  # Overwritten to provide flexibility for people who aren't using Rails.
  task :setup, :except => { :no_release => true } do
    run "cd #{deploy_to}; mkdir shared; mkdir releases"
    run "cd #{deploy_to}/#{shared_dir}; mkdir pids; mkdir assets; mkdir cache; mkdir logs;"
    run "cd #{deploy_to}/#{shared_dir}; chmod 777 assets; chmod 777 cache; chmod 777 logs"
  end

  # Each of the following tasks are Rails specific. They're removed.
  task :cold do
  end

  task :start do
  end

  task :stop do
  end
  
  task :branch_select do
    unless environment == 'production'
      set(:branch) { Capistrano::CLI.ui.ask("Branch: ") }
    end
  end

  # Symlink files dirs and clear cache directories after code updated.
  task :restart do
    run "export NODE_ENV='production'", roles: :web
    run "sudo forever stopall; cd /home/ubuntu/ConnR/current; sudo forever start app.js", roles: :web
    #run "sudo service apache2 reload", roles: :web
    #run "sudo /etc/init.d/solr restart", roles: :web
    # run "wget -O - -q http://#{domain}/clearapc.php"
  end

end
