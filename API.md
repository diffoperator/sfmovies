
APIs
====

1) movies

 a) /movie

    Parameters: movie (name of movie)

    Returns: A JSON list of all locations details associated with a movie

    Example: /movie?movie=Zodiac

 b) /movie/location

    Parameters: movie

    Returns: A JSON list of lat/long coordinates associated with a movie

    Example: /movie/location?movie=Zodiac

 c) /movie/release

    Parameters: release (integer number specifying the year of release)

    Returns: A JSON list of all location details of all movies released in a year

    Example: /movie/release?release=2014

 d) /movie/studio

    Parameters: studio (name of studio or production house)

    Returns: A JSON list of all location details of all movies associated withh the given studio

    Example: /movie/studio?studio=Paramount

  e) /movie/all

    Returns: A JSON list of all location details of all the movies (used mainly for debug purposes)

  f) /movie/all/names

    Returns a JSON list of only movie names of all movies. Used mainly for UI purposes.

  g) /movie/location/radius

    Parameters: latitude, longitude, limit (optional)

    Returns: A JSON list of all location details of movies shot near a given location

    Example: /movie/location/radius?latitude=37&longitude=-122&limit=5
