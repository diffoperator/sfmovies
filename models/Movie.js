var Mongoose = require('mongoose');

exports.Movie = new Mongoose.Schema({
        name : {
	    type: String,
	    required: true
	},
        studio : {
	    type: String,
	    required: false
	},
	release : {
	    type: Number,
	    required: false
	},
	address : {
	    type: String,
	    required: false
	},
        created_at : {
	    type: Date,
	    required: false
	},
	updated_at : {
	    type: Date,
	    required: false
	},
	location : {
	    type : [],
	    required: false
	}
    });

exports.Movie.index({location : '2d'});
